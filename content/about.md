+++
title = "About the HACKarnaval"
+++

“Hacking” is a practice of détournement, covering many technical cultures across various domains of expertise: from biology to analog and digital electronics, textiles, music, arts and crafts, hardware and software. Likewise, "carnaval" is much more than a traditional party, but a ritual of inversion which allows for new perspectives on how social and technical worlds are organized and experienced.

HACKarnaval brings these two concepts together to celebrate "hacking" as a form of knowledge sharing: we will bring together technologists, researchers, designers, artists, and curious tinkerers from various parts of the world to offer workshops, talks, and exhibitions on the practical art of software and hardware exploration for positive social change.

## Where ?

Join us at the Musée des Arts et Métiers! 

Join us at the afterparty at Jardin d'Alice!

* May 17, 2018 *

Free for students all day! 

Free for everyone after 18:00!

## Who ?

The event is organized by the community and supported by the following research laboratories and organizations: Musée des Arts et Métiers, CNAM; Institut Francilien Recherche Innovation Société (IFRIS); Laboratoire de Sociologie Économique, CNAM-CNRS.

The following organizations and projects are going to be present: DataPaulette hackerspace; Le Jack hackerspace; Framasoft; La Quadrature du Net; Free Software Foundation, Europe; Open Knowledge International; Centre for Research and Interdisciplinarity; LISE CNAM-CNRS,  Riseup.net; Laboratoire interdisciplinaire Sciences Innovations Société (LISIS - Université Paris Est Marne la Vallée); PING (?); Software Heritage project?

We will also have permanent hackerspace exhibitions:

* DataPaulette: textile experimentations

* Le Jack: retrocomputing hacking

* La Myne: do-it-yourself biohacking

## Thanks

The organizing committee is composed by: [list everyone] 

This site is based on Hugo v0.38 + 'vec' theme. For more info, check the repository of the project.
