+++
title = "Schedule"
id="schedule"
+++

| Time  | Salle 1: Decentralization and Security | Salle 2: Libre Hardware | Salle 3: Libre Software | Salle 4: Research and Development | Salle 5: Arts and Crafts | Salle de conférences |
|-------|---|---|---|---|---|---|
| 14:00 | "LEAP security platform" Micah Anderson (Riseup.net) EN | "Documenting Open Hardware: workshop" Tobias Wenzel (JOH) EN | "Software Heritage Project" Zach (INRIA) FR | "DIY Bio research" Morgan Meyer (ParisTech) FR | "E-textile hacking" DataPaulette FR |  |
| 15:00 | "Users' Computing Freedom: dispatches from the GnuPG project" Niibe Yutaka (Free Software Initiative Japan) EN | | "Retrocomputing: fighting programmed obsolecence" Acathla (Le Jack hackerspace) FR | | | "Culture and Politics of Hacking" Amaelle Guitton (Liberation) and Biella Coleman (McGill U.) EN + FR |
| 16:00 | "Introducing Tomu: workshop" (TBD) EN | "Circuit Design Workshop: Learning KiCad" (TBD) FR | "Biblioteques sans frontieres: hacking free software for access to knowledge" (TBD) FR | ["Anti-Eviction mapping: workshop"](../anti-eviction-mapping/) Erin McEroy (U. of California, Santa Cruz) EN | "Dead Minitel Orchestra" FR | "Un monde sans GAFA" Quadrature du Net, Framasoft FR |
| 17:00 | | "Assembling and configuring your GPG crypto token"  Gniibe (Flying Stone tech, Japan) & unixjazz (canudOS) EN | "Learning SDR: Software-Defined-Radio" (TBD) FR | "Human Computer InterHacktion, using tech to involve humans in art" Cedric Honnet (DataPaulette & Jack hackerspace) FR | "ANTIDATAMINING" RyBN.org art collective | |
| 18:00 |  | ["TV-B-GONE hands-on workshop, 1/2"](../tv-be-gone/) Mitch Altman (Noisebridge) EN + FR | Project Purism (TBD) FR | "Project Hackur.io" Biella Coleman (McGill U.) EN | | Book launch: "Makers: une monde sociale en mouvement" Marie-Christine Bureau, Isabelle Berrebi-Hoffmann, Michel Lallement (CNAM-LISE/CNRS) FR |
| 19:00 |  | ["TV-B-GONE hands-on workshop, 2/2"](../tv-be-gone/) Mitch Altman (Noisebridge) EN + FR | "Coding Circuit Designs: workshop" Kasper (Kitspace.org) EN | "Techniques and Politics of Big Data" Emannuel Didier (EHESS), Marc Barbier (UPEM)  FR |  | "Blockchain and the Law: book launch" Primavera DiFilipi (CNRS) FR | 
| 20:00 | "TAILS: The Amnesic Incognito Live System" (Tails team) FR | Recup: recyclage electronique (Martin Bobel + La Petite Rockette) FR | "Libreboot: workshop" Tech-Etical, Romania EN | "What can we learn from the history of UNIX in France?" (CNAM-History) FR | "Media art-ivism: de Mai-68 au hacktivism" (Charlie de Nose) FR | "Hacking: debating (mis)appropriations" FR |  
| 22:00 | Afterparty: Jardin d'Alice |
