+++ 
title = "TV-B-GONE workshop" 
id = "tv-be-gone"
+++

## Mitch Altman (Cornfield Electronics, Noisebridge)

**Description:** Ever been distracted by TVs in public places and wish you
could turn them off?  Now you can!  With the TV-B-Gone you can turn off TVs up
to 50 meters away!  In this workshop everyone (even people who have never made
anything -- any age 8 years or older) will learn to solder by making their own
TV-B-Gone remote control.  No one will ever know it's you making rooms into more
peaceful places for everyone.

**Language:** English and French.

