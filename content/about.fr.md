+++ 
title = "A propos du HACKarnaval" 
url = "/a-propos" 
+++

"Hacker" c'est une pratique de détournement: de la biologie à l’électronique,
du software au hardware, des arts textiles à l’artisanat, de la musique, etc.
De même, le "carnaval" est un rituel d’inversion de l’ordre qui permet
d’explorer et d’expérimenter de nouvelles façons d’appréhender le monde.

HACKarnaval intègre ces deux concepts pour célébrer le "hacking" comme une
forme de partage des connaissances. Des techniciens, des chercheurs, des
designers, des artistes et des bricoleurs de tous les coins du monde
proposeront des ateliers, des conferences, et des expositions. Venez explorer
les mondes socio-techniques du hardware, du wetware, et du software et discuter
des changements qu’ils promettent.  

## Où et quand ?

* Mai 17, 2018 *
 
De 14 à 21 heures au Musée des Arts et Métiers. Entrée gratuite pour les
étudiants. Après les 18:00, entrée gratuite pour tous.

## Qui ?

Soutien financier : Musée des Arts et Métiers, CNAM; Institut Francilien
Recherche Innovation Société (IFRIS); Laboratoire de Sociologie Économique,
CNAM-CNRS. Laboratoire interdisciplinaire Sciences Innovations Société (LISIS -
Université Paris Est Marne la Vallée).

Soutient : Mozilla fondation.

Intervenants : DataPaulette hackerspace; Le Jack hackerspace; Framasoft; La
Quadrature du Net; Free Software Foundation, Europe; Open Knowledge
International; Centre for Research and Interdisciplinarity; Riseup.net; PING
(sous réserve); Software Heritage project (sous réserve)

Expositions: DataPaulette: textile experimentations, Le Jack: retrocomputing
hacking ; La Myne: do-it-yourself biohacking

Le comité d’organisation : [list everyone]

Ce site est basé sur Hugo v0.32 + 'vec' thème.


































