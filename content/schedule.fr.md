+++
title = "Programme"
url = "/programme"
+++

| Time  | Salle 1: Decentralization and Security | Salle 2: Libre Hardware | Salle 3: Libre Software | Salle 4: Research and Development | Salle 5: Arts and Crafts | Salle de conférences |
|-------|---|---|---|---|---|---|
| 14:00 | "LEAP security platform" Micah Anderson (Riseup.net) EN | "Documenting Open Hardware: workshop" Tobias Wenzel (JOH) EN | "Software Heritage Project" Stefano Z. (INRIA) FR | "DIY Bio research" Morgan Meyer (ParisTech) FR | "E-textile hacking" DataPaulette FR | "Community Mesh-networking in Germany" Juergen Neumann (Freie Funk) EN |
| 15:00 | "Users' Computing Freedom: dispatches from the GnuPG project" Niibe Yutaka (Free Software Initiative Japan) EN | "Build your own microscope" Richard Bowman (U. of Bath, UK) EN | "Retrocomputing: fighting programmed obsolecence" Acathla et Barzi (Le Jack hackerspace) FR | "Studying Free Software: sociotechnical questions" Gabriel Alcaras (ENS) FR | "Kobakant: textile exploration" Mika and Hannah EN | "Culture and Politics of Hacking" Amaelle Guitton (Lib) and Biella Coleman (McGill U.) EN + FR |
| 16:00 |  | "Introducing Tomu: workshop" (?) FR | "VLC: multimedia development plaform: a primer" Jean-Batiste Kampf (VLC.org) FR | ["Anti-Eviction mapping: workshop"](../anti-eviction-mapping-fr/) Erin McEroy (U. of California, Santa Cruz) EN | | "Biblioteques sans frontieres: hacking free software for access to knowledge" (?) FR | 
| 17:00 | | "Assembling and configuring your GPG crypto token"  Gniibe (Flying Stone tech, Japan) & unixjazz (canudOS) EN | "Learning SDR: Software-Defined-Radio" (?) FR | "From InterHacktivity to InterArtivity: HCI research" Drix Honnet (Le Jack) FR | | "Data Politics: a debate" Emannuel Didier (EHESS), Marc Barbier (UPEM), OpenEditions Editors FR |
| 18:00 |  | ["TV-B-GONE hands-on workshop, 1/2"](../tv-be-gone-fr/) Mitch Altman (Noisebridge) EN + FR | Project Purism (?) FR | "Hackademia: studying hacking in academia" or "Project Hackur.io" Biella Coleman (McGill U.) EN | "Building sensing tech for shared workshops: an HCI study" Yvonne Jansen (UPMC, Paris) FR | Book launch: "Makers: une monde sociale en mouvement" Marie-Christine Bureau, Isabelle Berrebi-Hoffmann, Michel Lallement (CNAM-LISE/CNRS) FR |
| 19:00 |  | ["TV-B-GONE hands-on workshop, 2/2"](../tv-be-gone-fr/) Mitch Altman (Noisebridge) EN + FR | "Coding Circuit Designs: workshop" Kasper (Kitspace.org) EN |
| 20:00 | "TAILS: The Amnesic Incognito Live System" (Tails team) FR | "Libreboot: workshop" Techetical EN | | "What can we learn from the history of UNIX in France?" (CNAM-History) FR | |  
| 22:00 | Afterparty: Jardin d'Alice |
