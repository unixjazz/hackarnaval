+++ 
title = "Contact" 
url = "/contact-fr"
+++

Behind the email address, there are multitudes. Contact us at
**'hackarnaval!SPAM' at 'riseup.net'** Varios members of the organization share
this account. Alternatively, you can spam everyone involved using the
mailing-list: **'hackarnaval!SPAM' at 'lists.riseup.net'**

Remember, folks, with great computing power comes great responsibilities.
HACKarnaval is a safe space and we want to keep it this way. We are open to all, regardless of expertise level, socioeconomic status, nationality, language, gender, ethnic origin, etc. No discriminatory language or behavior will be accepted. If you experience or witness any form of disrespect, please report to the organizers using the email address above. We will promptly respond while striving to maintain your privacy and anonymity.

**PGP fingerprint:** 8C79 08B4 A211 07AF 11B5 737D F073 2DAB 6AC0 D6E1 

**Signal:** Ask us.


