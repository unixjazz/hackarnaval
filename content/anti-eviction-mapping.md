+++ 
title = "Anti-Eviction Mapping: Workshop" 
id = "anti-eviction-mapping" 
+++

## Erin McEroy (U. of California, Santa Cruz) 

**Description:** This workshop will be dedicated to the presentation of the
tools and techniques of the Anti-Eviction Mapping Project (AEMP)
(http://www.antievictionmap.com) - a data visualization, data analysis, and
digital storytelling collective documenting dispossession and resistance upon
gentrifying US landscapes. By collaboratively performing research and gathering
oral histories in San Francisco Bay Area, New York City, and Los Angeles, the
AEMP works with an array of community partners, providing analyses and
visualizations useful for on-the-ground housing justice efforts. As a
technology project itself, the AEMP endeavors to rethink technological
possibilities and futures upon a landscape ravaged by the entwining of
technocapitalism, speculative real estate, and racist housing policy. In this
talk, Erin McElroy from the AEMP will share the collective’s work and methods,
focusing on the San Francisco Bay Area region. Time will also be spent to think
through what methods might be instructive to new data research being performed
housing activists elsewhere, and how community-derived data work can inform
direction action tactics and strategizing. 

**Language:** English.

